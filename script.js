/* Відповідь 1:
forEach: дає зручний спосіб проходитись по елементах масиву та виконувати дії з ними без необхідності створювати цикл вручну.
 */

/* Відповідь 2:
1)Присвоїти порожній масив [].
2)Встановити властивість length масиву на 0.
3)Використати цикл while з методом pop().
4)Використати метод splice(0).
5)Використати метод length з циклом for.
6)Використати метод length з циклом for...of.
7)Використати метод length з методом forEach().
8)Використати метод length з методом filter().
9)Використати метод length з методом map().
10)Використати метод length з методом reduce().
11)Використати метод length з методом shift().
12)Використати метод length з методом splice().
13)Використати метод length з методом unshift().
14)Використати метод length з оператором while.
15)Використати метод length з оператором do...while.
*/

/* Відповідь 3:
!Використання Array.isArray().!
Використання оператора instanceof.
Перевірка за допомогою constructor.
Використання Object.prototype.toString.call().
*/

// Задача:

function filterBy(arr, dataType) {
  return arr.filter((item) => typeof item !== dataType);
}

const originalArray = ["hello", "world", 23, "23", null];
const filteredArray = filterBy(originalArray, "string");
console.log(filteredArray);
